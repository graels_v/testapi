<?php
/**
 * Created by PhpStorm.
 * User: zx-coder
 * Date: 08.02.16
 * Time: 17:17
 */

class OauthClient
{
    const HTTP_LOCATION_CODE = 302;

    /** @var  Client */
    private $client;

    public function __construct()
    {
        $this->client = new Client();
    }

    /**
     * @param string $domain
     *
     * @return Response
     */
    private function requestOauth($domain, &$isOk = false)
    {
        $isOk = false;
        $params = array(
            'response_type' => 'code',
            'client_id' => CLIENT_ID,
            'redirect_uri' => REDIRECT_URI,
        );

        $path = '/oauth/authorize/';

        $redirectUrlData = parse_url(REDIRECT_URI);

        $response = $this->client->execute(PROTOCOL . '://' . $domain . $path, $params);
        $httpLocationCode = $response->getHttpCode();
        while ($httpLocationCode === self::HTTP_LOCATION_CODE) {
            $locationUrlData = parse_url($response->getLocationUrl());
            if ($locationUrlData['host'] === $redirectUrlData['host']) {
                $isOk = true;
                return $response;
            }
            $response = $this->client->execute($response->getLocationUrl());
            $httpLocationCode = $response->getHttpCode();

        }

        return $response;
    }

    /**
     * @param string $url
     * @param string $login
     * @param string $password
     * @return Response
     */
    private function login($url, $login, $password)
    {
        $params = array(
            'USER_LOGIN' => $login,
            'USER_PASSWORD' => $password,
            'AUTH_FORM'  => 'Y',
            'TYPE' => 'AUTH',
        );

        $response = $this->client->execute($url, $params, true);
        $httpLocationCode = $response->getHttpCode();
        while ($httpLocationCode === self::HTTP_LOCATION_CODE) {
            $response = $this->client->execute($response->getLocationUrl());
            $httpLocationCode = $response->getHttpCode();
        }

        return $response;
    }

    public function getCodeData($domain)
    {
        $response = $this->requestOauth($domain, $isOk);
        $this->login($response->getLocationUrl(), 'agavrin@gmail.com', '9Os9Tm');
        $response = $this->requestOauth($domain, $isOk);

        if ($isOk === true) {
            $urlData = parse_url($response->getLocationUrl());
            if (array_key_exists('query', $urlData)) {
                parse_str($urlData['query'], $codeData);
                return $codeData;
            }
        }

        return null;
    }



}
