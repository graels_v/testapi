<?php
/**
 * Created by PhpStorm.
 * User: zx-coder
 * Date: 08.02.16
 * Time: 16:30
 */

class Response
{
    /** @var  int */
    private $httpCode;
    /** @var  string */
    private $originalUrl;
    /** @var  string */
    private $locationUrl;
    /** @var  string */
    private $data;

    /**
     * @param int $httpCode
     * @param string $originalUrl
     * @param string $locationUrl
     * @param string $data
     */
    public function __construct(
        $httpCode,
        $originalUrl,
        $locationUrl,
        $data
    )
    {
        $this->httpCode    = (int)$httpCode;
        $this->originalUrl = $originalUrl;
        $this->locationUrl = $locationUrl;
        $this->data        = $data;
    }

    /**
     * @return string
     */
    public function getLocationUrl()
    {
        return $this->locationUrl;
    }

    /**
     * @return string
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * @return string
     */
    public function getOriginalUrl()
    {
        return $this->originalUrl;
    }

    /**
     * @return int
     */
    public function getHttpCode()
    {
        return $this->httpCode;
    }
}
