<?php

/**
 * Created by PhpStorm.
 * User: zx-coder
 * Date: 08.02.16
 * Time: 15:52
 */
class Client
{
    /** @var array  */
    private $cookies = [];

    /**
     * @param string $url
     * @param array $params
     * @param boolean $isPost
     *
     * @return Response
     */
    public function execute($url, $params = [], $isPost = false)
    {
        $cookies = $this->getCookiesByUrl($url);
        $ch = curl_init();

        if (true === $isPost) {
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
        } else if (count($params) > 0) {
            $url .= strpos($url, '?') > 0 ? '&' : '?';
            $url .= http_build_query($params);
        }

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HEADER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_HEADER, 1);
        if (count($cookies) > 0) {
            curl_setopt($ch, CURLOPT_COOKIE, implode(';', $cookies));
        }

        $data = curl_exec($ch);

        $httpCode    = curl_getinfo($ch, CURLINFO_HTTP_CODE);

        preg_match_all('/^Set-Cookie:\s*([^;]*)/mi', $data, $cookieMatches);
        $setCookies = [];
        if ($cookieMatches) {
            $setCookies = $cookieMatches[1];
        }

        $this->setCookiesByUrl($url, $setCookies);
        $locationUrl = $this->getLocationUrl($url, $data);
        curl_close($ch);

        return new Response($httpCode, $url, $locationUrl, $data);
    }

    /**
     * @param string $url
     * @param array $cookies
     */
    public function setCookiesByUrl($url, array $cookies) {
        $urlData = parse_url($url);
        $hostName = $urlData['host'];

        if (array_key_exists($hostName, $this->cookies)) {
            $this->cookies[$hostName] = array_merge($cookies, $this->cookies[$hostName]);
        } else {
            $this->cookies[$hostName] = $cookies;
        }
    }

    /**
     * @param string $url
     * @return array
     */
    public function getCookiesByUrl($url)
    {
        $urlData = parse_url($url);
        $hostName = $urlData['host'];

        if (array_key_exists($hostName, $this->cookies)) {
            return $this->cookies[$hostName];
        } else {
            return [];
        }
    }

    /**
     * @param string $url
     * @param string $data
     * @return string
     */
    private function getLocationUrl($url, $data)
    {
        preg_match_all('/^Location:\s*([^;\n\r]*)/mi', $data, $locationMatches);
        $locationUrl = $url;
        if (isset($locationMatches[1][0])) {
            $locationUrl = $locationMatches[1][0];

            $locationUrlData = parse_url($locationUrl);
            if (!array_key_exists('host', $locationUrlData) && array_key_exists('path', $locationUrlData)) {
                $urlData = parse_url($url);
                $locationUrl = $urlData['scheme'] . '://' . $urlData['host'] . $locationUrlData['path'];
            }
        }

        return $locationUrl;
    }
}
